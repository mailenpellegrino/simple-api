from qg_ui import create_app
from qg_ui import settings

app = create_app()


if __name__ == "__main__":
    app.run(host=settings.FLASK_SERVER_HOST,
        port=settings.FLASK_SERVER_PORT,
        debug=settings.FLASK_DEBUG)
