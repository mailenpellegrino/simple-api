FROM python:3.10.4

ARG HOME=/simple-api

RUN mkdir ${HOME}
RUN mkdir ${HOME}/.log
COPY . ${HOME}

WORKDIR ${HOME}

RUN pip3 install -r requirements.txt

ENTRYPOINT ["python3", "web.py"]

