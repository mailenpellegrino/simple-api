from transformers import BartForConditionalGeneration, BartTokenizer
import sys



model = BartForConditionalGeneration.from_pretrained('facebook/bart-large-cnn')
tokenizer = BartTokenizer.from_pretrained('facebook/bart-large-cnn')

def summarize_text(text):
    input_ids = tokenizer.encode(text, return_tensors='pt', max_length=1024, truncation=True)
    resumen_ids = model.generate(input_ids, max_length=150, min_length=40, length_penalty=2.0, num_beams=4, early_stopping=True)
    summary = tokenizer.decode(resumen_ids[0], skip_special_tokens=True)
    return summary

def get_response(user_query):

    summary = summarize_text(user_query)
    final_response = 'Your summary is: ' + summary 
    return final_response


