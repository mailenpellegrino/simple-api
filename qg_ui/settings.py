import os

# Flask settings
FLASK_SERVER_HOST = '0.0.0.0' # 'localhost'
FLASK_SERVER_PORT = 5002
FLASK_DEBUG = True  # Do not use debug mode in production


def get(var_name): 
    value = os.getenv(var_name)
    if value == None:
        value = eval(var_name)
    return value

