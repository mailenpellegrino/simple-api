import csv
import json
import logging
import requests
from requests.models import requote_uri
from pprint import pformat
import wtforms
from flask import current_app as app
from flask import  send_from_directory
from flask import flash, render_template, request,redirect,url_for,make_response, Flask,session
from qg_ui import settings
import numpy as np
from datetime import datetime
import smtplib,ssl
import os
from flask import jsonify
from utils.backend import *


log = logging.getLogger('summarizer')



@app.route("/", methods=['GET','POST'])
def index():   
    log.info(pformat(request.headers))
    return render_template('index.html')

@app.route("/form", methods=['GET','POST'])
def search_form(): 
    
    return render_template('form.html')


@app.route('/result', methods=['GET','POST'])
def result():
    result = None 
    if request.method == 'POST':
        question = request.form['question']
        result = get_response(question)
    return render_template('result.html', result=result)
        
@app.route('/static/<path:filename>')
def serve_static(filename):
    return send_from_directory('static', filename)

