import logging.config
import logging.handlers
import os
from flask import Flask
from flask_cors import CORS
from werkzeug.middleware.proxy_fix import ProxyFix

def create_app():
    app = Flask(__name__)
    app.config.from_object(__name__)
    app.config['SECRET_KEY'] = 'SjdnUends821Jsdlkvxh391ksdODnejdDw'
    
    app.wsgi_app = ProxyFix(app.wsgi_app, x_proto=1, x_port=1, x_for=1, x_host=1, x_prefix=1)

    CORS(app)  # Accessible from anywhere
    logging_conf_path = os.path.normpath(os.path.join(os.path.dirname(__file__), 'logging.conf'))
    logging.config.fileConfig(logging_conf_path)
    log = logging.getLogger('summarizer')

    with app.app_context():
        from qg_ui import routes
        return app
